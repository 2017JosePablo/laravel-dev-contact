<?php

Route::get('/', function () {
    return view('inicio.welcome');
});
Route::get('/index', function () {
    return view('inicio.welcome');
});

Route::POST('/iniciar', ['uses'=>'HomePageController@iniciar']);

Route::get('/contactos','ContactosController@contactos');

Route::get('/registro','RegistroController@index');

Route::POST('/regUsuario', ['uses'=>'RegistroController@registrar']);

Route::POST('/getContact','ContactosController@getContactos');

Route::POST('/registerContact',['uses'=>'ContactosController@registrarContacto']);

Route::POST('/actualizarContact',['uses'=>'ContactosController@actualizarContacto']);

Route::POST('/deleteContact',['uses'=>'ContactosController@eliminarContacto']);

Route::POST('/deleteSesion','HomePageController@elimarSesion');

Route::get('/perfil','RegistroController@miPerfil');

Route::POST('/infoPerfil','RegistroController@getInfoLogin');

Route::POST('/actualizarPersona',['uses'=>'RegistroController@actualizarPersona']);
