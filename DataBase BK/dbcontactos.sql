-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-01-2019 a las 23:38:10
-- Versión del servidor: 10.1.30-MariaDB
-- Versión de PHP: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `dbcontactos`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `actualizarPersona` (IN `id` INT(11), IN `ced` INT(11), IN `nom` VARCHAR(30), IN `ap1` VARCHAR(30), IN `ap2` VARCHAR(30), IN `corr` VARCHAR(30), IN `tele` INT(10), IN `fech` DATE, IN `gen` INT(11))  NO SQL
BEGIN
	UPDATE tbpersona SET tbpersona.cedula = ced ,tbpersona.nombre = nom,tbpersona.apellido1 = ap1,tbpersona.apellido2 = ap2,tbpersona.correo = corr,tbpersona.telefono = tele,tbpersona.fechaNacimiento = fech,tbpersona.genero = gen WHERE tbpersona.idPersona = id;
 
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteContact` (IN `id` INT(11))  NO SQL
BEGIN
	DELETE FROM tbpersona WHERE tbpersona.idPersona = id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getContactos` ()  NO SQL
BEGIN
	SELECT tbpersona.idPersona AS id,tbpersona.cedula AS ced,tbpersona.nombre AS nom,tbpersona.apellido1 AS ap1,tbpersona.apellido2 AS ap2,tbpersona.correo AS corr, tbpersona.telefono AS tel,tbpersona.fechaNacimiento AS fec,tbpersona.genero AS gen FROM tbpersona	LEFT JOIN tblogin ON tbpersona.idPersona = tblogin.usuario WHERE tblogin.usuario IS NULL;
    
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getIdUsuario` (IN `ced` INT(11))  NO SQL
BEGIN
	SELECT tbpersona.idPersona FROM tbpersona WHERE tbpersona.cedula = ced;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getInfoPerfil` (IN `id` INT(11))  NO SQL
BEGIN

SELECT 
tbpersona.idPersona AS id,tbpersona.cedula AS ced,tbpersona.nombre AS nom,tbpersona.apellido1 AS ap1,tbpersona.apellido2 AS ap2,tbpersona.correo AS corr, tbpersona.telefono AS tel,tbpersona.fechaNacimiento AS fec,tbpersona.genero AS gen, tblogin.contrasena AS contr 

FROM 
tbpersona RIGHT JOIN tblogin ON tbpersona.idPersona = tblogin.usuario WHERE tblogin.usuario = id;


END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getLogin` (IN `ced` INT(11), IN `cont` VARCHAR(50))  NO SQL
BEGIN

SELECT tblogin.idLogin AS id , tblogin.rol AS rol, tbpersona.idPersona AS idPer, concat(tbpersona.nombre," ",tbpersona.apellido1," ",tbpersona.apellido2) AS nombre FROM tblogin INNER JOIN tbpersona ON tblogin.usuario = tbpersona.idPersona WHERE tbpersona.cedula = ced AND tblogin.contrasena = cont;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertLogin` (IN `idPer` INT(11), IN `contr` VARCHAR(50), IN `rol` INT(11))  NO SQL
BEGIN
	INSERT INTO tblogin (tblogin.usuario,tblogin.contrasena,tblogin.rol) VALUES(idPer,contr,rol);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `registrarPersona` (IN `ced` INT(11), IN `nom` VARCHAR(30), IN `ap1` VARCHAR(30), IN `ap2` VARCHAR(30), IN `corr` VARCHAR(30), IN `tele` INT(10), IN `fech` DATE, IN `gen` INT(11))  NO SQL
BEGIN
	INSERT INTO tbpersona(tbpersona.cedula,tbpersona.nombre,tbpersona.apellido1,tbpersona.apellido2,tbpersona.correo,tbpersona.telefono,tbpersona.fechaNacimiento,tbpersona.genero) VALUES (ced,nom,ap1,ap2,corr,tele,fech,gen);
 
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateLogin` (IN `idPer` INT(11), IN `contr` VARCHAR(50), IN `rol` INT(11))  NO SQL
BEGIN
	UPDATE tblogin SET tblogin.contrasena = contr,tblogin.rol = rol WHERE tblogin.usuario = idPer;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblogin`
--

CREATE TABLE `tblogin` (
  `idLogin` int(11) NOT NULL,
  `usuario` int(11) NOT NULL,
  `contrasena` varchar(50) NOT NULL,
  `rol` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tblogin`
--

INSERT INTO `tblogin` (`idLogin`, `usuario`, `contrasena`, `rol`) VALUES
(1, 3, 'admin', 1),
(2, 6, 'admin', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbpersona`
--

CREATE TABLE `tbpersona` (
  `idPersona` int(11) NOT NULL,
  `cedula` int(11) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `apellido1` varchar(30) NOT NULL,
  `apellido2` varchar(30) NOT NULL,
  `correo` varchar(30) NOT NULL,
  `telefono` int(10) NOT NULL,
  `fechaNacimiento` date NOT NULL,
  `genero` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbpersona`
--

INSERT INTO `tbpersona` (`idPersona`, `cedula`, `nombre`, `apellido1`, `apellido2`, `correo`, `telefono`, `fechaNacimiento`, `genero`) VALUES
(3, 504130763, 'Jose Pablo', 'Carranz', 'Alfaro', 'carranzap-1996@hotmail.com', 50088481, '2018-06-21', 1),
(6, 111111111, 'Juan Benito', 'Carranza', 'Bermudez', 'carranza@yahoo.com', 87899598, '1972-03-30', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbrol`
--

CREATE TABLE `tbrol` (
  `idrol` int(11) NOT NULL,
  `nombre` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbrol`
--

INSERT INTO `tbrol` (`idrol`, `nombre`) VALUES
(1, 'administrador'),
(2, 'user'),
(3, 'super admin');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tblogin`
--
ALTER TABLE `tblogin`
  ADD PRIMARY KEY (`idLogin`),
  ADD KEY `usuario` (`usuario`) USING BTREE,
  ADD KEY `rol` (`rol`);

--
-- Indices de la tabla `tbpersona`
--
ALTER TABLE `tbpersona`
  ADD PRIMARY KEY (`idPersona`),
  ADD UNIQUE KEY `cedula` (`cedula`),
  ADD KEY `idPersona` (`idPersona`);

--
-- Indices de la tabla `tbrol`
--
ALTER TABLE `tbrol`
  ADD PRIMARY KEY (`idrol`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tblogin`
--
ALTER TABLE `tblogin`
  MODIFY `idLogin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tbpersona`
--
ALTER TABLE `tbpersona`
  MODIFY `idPersona` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `tbrol`
--
ALTER TABLE `tbrol`
  MODIFY `idrol` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `tblogin`
--
ALTER TABLE `tblogin`
  ADD CONSTRAINT `tblogin_ibfk_1` FOREIGN KEY (`usuario`) REFERENCES `tbpersona` (`idPersona`),
  ADD CONSTRAINT `tblogin_ibfk_2` FOREIGN KEY (`rol`) REFERENCES `tbrol` (`idrol`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
