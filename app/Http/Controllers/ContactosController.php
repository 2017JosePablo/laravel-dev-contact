<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\Controller;

class ContactosController extends Controller
{
    public function contactos()
    {
      return view('contactos.contactos');
    }
    public function getContactos(){
      $contactos = DB::select('call getContactos');
      return $contactos;
    }
    public function actualizarContacto(Request $request){

      //$id = $request->session()->get('datos')[0]->idPer;
      try {
          if(DB::update('call actualizarPersona(?,?,?,?,?,?,?,?,?)',array($_POST['id'],$_POST['cedula'],$_POST['nombre'],$_POST['apellido1'],$_POST['apellido2'],$_POST['correo'],$_POST['telefono'],$_POST['fecha'],$_POST['genero']))){
            return 'true';
          }else{
            return 'false';
        }
      } catch (\Exception $e) {
      }
    }
    public function registrarContacto(Request $request){
      try {
          if(DB::insert('call registrarPersona(?,?,?,?,?,?,?,?)',array($_POST['cedula'],$_POST['nombre'],$_POST['apellido1'],$_POST['apellido2'],$_POST['correo'],$_POST['telefono'],$_POST['fecha'],$_POST['genero']))){
            return 'true';
          }else{
            return 'false';
        }
      } catch (\Exception $e) {
        return 'userExist';
      }
    }
    //deleteContact
    public function eliminarContacto(Request $request){
      try {
          if(DB::delete('call deleteContact(?)',array($_POST['id']))){
            return 'true';
          }else{
            return 'false';
        }
      } catch (\Exception $e) {
        return 'error';
      }
    }
}
