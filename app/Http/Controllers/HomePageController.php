<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\Controller;

use Tracker;
use Cookie;
use Session;

class HomePageController extends Controller
{
    public function home(){
		    return view('welcome');
    }
    public function iniciar(Request $request){
      $datosLogin = DB::select('call getLogin(?,?)',array($_POST['usuario'],$_POST['contrasena']));
      if($datosLogin){
        $request->session()->put('datos',$datosLogin);
        return 'true';
      }else{
        return "false";
      }
    }
    public function elimarSesion(Request $request){
      $request->session()->flush();
      $request->session()->forget('datos');
    }
}
