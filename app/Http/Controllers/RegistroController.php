<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\Controller;

class RegistroController extends Controller
{
    public function index()
    {
      return view('inicio.registro');
    }
    public function registrar(Request $request){
      try {
         if(DB::insert('call registrarPersona(?,?,?,?,?,?,?,?)',array($_POST['cedula'],$_POST['nombre'],$_POST['apellido1'],$_POST['apellido2'],$_POST['correo'],$_POST['telefono'],$_POST['fecha'],$_POST['genero']))){

           $id = DB::select('call getIdUsuario(?)',array($_POST['cedula']));

           if(DB::insert('call insertLogin(?,?,?)',array($id[0]->idPersona,$_POST['contrasena'],$_POST['rol']))){
             return 'true';
           }else{
             return 'notLogin';
           }
         }else{
           return 'notInserted';
         }
      } catch (\Exception $e) {
        return 'userExist';
      }
  }

  public function miPerfil(Request $request){
      return view('contactos.perfil');
  }
  public function getInfoLogin(Request $request)
  {
    return DB::select('call getInfoPerfil(?)' , array($request->session()->get('datos')[0]->idPer));
  }

  public function actualizarPersona(Request $request){
    try {
      //if change the password
      $pass = '';
        if($_POST['state'] == "true"){
          if(DB::update('call updateLogin(?,?,?)',array($request->session()->get('datos')[0]->idPer,$_POST['cont'],$request->session()->get('datos')[0]->rol))){
              $pass = 'true';
          }else{
              $pass = 'true';
          }
        }
        $user = '';
        if(DB::update('call actualizarPersona(?,?,?,?,?,?,?,?,?)',array($request->session()->get('datos')[0]->idPer,$_POST['cedula'],$_POST['nombre'],$_POST['apellido1'],$_POST['apellido2'],$_POST['correo'],$_POST['telefono'],$_POST['fecha'],$_POST['genero']))){
          $user = 'true';
        }else{
          $user = 'false';
      }
    } catch (\Exception $e) {
      $user = $e;
    }
    return ['pass'=>$pass,'user'=>$user];
  }

}
