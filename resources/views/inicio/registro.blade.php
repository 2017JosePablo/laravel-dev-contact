@extends('layouts.app')

@section('meta')
  <meta name="_token" id="_token" content="{{ csrf_token() }}"  />
@endsection

@section('tittle')
  Registro
@endsection
@section('style')

@endsection

@section('content')
  <br>
  @include('contactos.registro')
@endsection

@section('script')
  <script type="text/javascript" src="{{ asset('js/JsFechas.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/JsRegistro.js') }}"></script>
@endsection
