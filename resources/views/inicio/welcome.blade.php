@extends('layouts.app')

@section('tittle')
Contactos
@endsection

@section('style')

@endsection
@section('content')
  <br>
  <div class="error">

  </div>
    <div class="row">
      <div class="col-md-6 col-md-offset-3" id="inicio">
        <h1 class="text-center">Inicio de Sesi&oacute;n</h1>
        <form class="form-horizontal" role="form" action="#"  method="post" id="form">
          <input type="hidden" name="" value="{{!! csrf_token() !!}}" >
          <div class="form-group">
            <div class="col-lg-8 col-lg-offset-2">
              <label class=""></label>
              <input class="form-control input" value="" type="text" id="usuario" name="usuario"  data-toggle="tooltip" data-placement="top" title="Usuario" placeholder="Cedula">
            </div>
          </div>
          <div class="form-group">
            <div class="col-lg-8 col-lg-offset-2">
              <label class=""></label>
              <input class="form-control input" value="" type="password" id="contra" name="contra" data-toggle="tooltip" data-placement="top" title="Contrase&ntilde;a" placeholder="Contrase&ntilde;a">
            </div>
          </div>
          <div class="form-group">
            <div class="col-lg-4 col-md-offset-2">
              <label class=""></label>
              <button type="button" class="btn btn-info form-control" name="button" id="reg">Registrarme</button>
            </div>
            <div class="col-lg-4 ">
              <label class=""></label>
              <button type="button" class="btn btn-success form-control" name="button" id="acceder">Acceder</button>
            </div>
          </div>
        </form>
      </div>
    </div>
@endsection
@section('prueba')
  @isset($_GET['success'])
    @if ($_GET['success'] == 'inserted')
      <script type="text/javascript">

        $(document).ready(function(){
          $('.error').html(makeMessage('success','Se han guardado los datos satisfactoriamente','Puedes iniciar sesi&oacute;n'));
        });
      </script>
    @endif
    @if ($_GET['success'] == 'inserted')
      <script type="text/javascript">
        $(document).ready(function(){
          $('.error').html(makeMessage('success','Se han guardado los datos satisfactoriamente','Puedes iniciar sesi&oacute;n'));
        });
      </script>
    @else
      @if ($_GET['success'] == 'close')
        <script type="text/javascript">
          $(document).ready(function(){
            $('.error').html(makeMessage('success','Exito','Se ha cerrado la sesi&oacute;n satisfactoriamente'));
          });
        </script>
      @endif
    @endif
  @endisset
  @isset($_GET['error'])
    @if ($_GET['error'] == 'notSesion')
      <script type="text/javascript">
        $(document).ready(function(){
          $('.error').html(makeMessage('error','Error','No tienes una sesi&oacute;n activa, debes de iniciar sesi&oacute;n para poder acceder a la pagina'));
        });
      </script>
    @endif
  @endisset
@endsection
@section('script')
  <script type="text/javascript" src="{{ asset('js/JsInicio.js') }}"></script>
@endsection
