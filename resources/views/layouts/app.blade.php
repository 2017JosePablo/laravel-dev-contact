<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
      <meta name="_token" id="_token" content="{{ csrf_token() }}"  />
        @yield('meta')
        <LINK REL="SHORTCUT ICON" HREF="https://cdn.icon-icons.com/icons2/827/PNG/512/user_icon-icons.com_66546.png">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>@yield('tittle','Contactos')</title>
        <script type="text/javascript" src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/jquery.validate.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/JsMessages.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/JsFechas.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/JsSesion.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/jquery.toast.js') }}"></script>

        <link rel="stylesheet" href="css/jquery.toast.css">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/footer.css">

        @yield('head')
        @yield('script')
        @yield('style')

        <style media="screen">

          #inicio{
            background: #EED;
            border-top:  3px solid #C40F0F;
            border-right:  3px solid #C40F0F;
            border-left:  1px solid #C40F0F;
            border-bottom:   1px solid #C40F0F;
            border-radius:10px;
          }
            #registro{
              border-top:  3px solid #C40F0F;
              border-right:  3px solid #C40F0F;
              border-left:  1px solid #C40F0F;
              border-bottom:   1px solid #C40F0F;
              border-radius:10px;
            }

          h1{
            text-decoration: underline;
          }
        </style>
    </head>
    <body>

        <div class="container" >

            @yield('nav')

            @yield('messages')
            @yield('content')
        </div>
        <footer class="footer">
          <div class="container">
            <p id="contenido">Carranza Alfaro, Contactos Laravel</p>
          </div>
        </footer>

    </body>
</html>
@yield('prueba')
