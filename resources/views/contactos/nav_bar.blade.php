<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Contactos</a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
        <li class="active"><a href="contactos">Home</a></li>

      </ul>
      <ul class="nav navbar-nav navbar-right">
        @if (Session::has('datos'))
          <li><a href="perfil" class="disable">{{Session::get('datos')[0]->nombre}}</a></li>
        @endif
        <li><a href="#" class="btn" id="closeSesion">Cerrar Session</a></li>
      </ul>
    </div>
  </div>
</nav>
