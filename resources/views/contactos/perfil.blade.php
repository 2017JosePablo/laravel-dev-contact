@extends('layouts.app')

@include('sesion.sesion')

@section('nav')
  @include('contactos.nav_bar')
@endsection

@section('content')
  @include('contactos.registro')
@endsection


@section('script')
  <script type="text/javascript" src="{{ asset('js/JsPerfil.js') }}"></script>
@endsection
