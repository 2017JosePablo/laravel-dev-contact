@extends('layouts.app')

@include('sesion.sesion')


@section('tittle')
  Contactos
@endsection

@section('nav')
  @include('contactos.nav_bar')
@endsection

@section('content')
    <div class="row">
      <div class="col-md-2">
        <p>Este Sitio es para almacenar informacion de los contactos</p>
      </div>
      <div class="col-md-3">
        <div class="panel panel-default" id="">
          <div class="panel-heading">
            <h2 class="panel-title">Contactos</h2>
          </div>
          <div class="panel-body">
            <table class=" table table-responsive">
              <thead>
                <th class="text-center" >Nombre</th>
              </thead>
              <tbody id="tbody">
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="col-md-7">
        @include('contactos.registro')
      </div>
    </div>
@endsection
@section('script')
  <script type="text/javascript" src="{{ asset('js/JsContactos.js') }}"></script>
  <script src="http://bootboxjs.com/bootbox.js"></script>
@endsection
