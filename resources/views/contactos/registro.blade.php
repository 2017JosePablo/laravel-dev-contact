<div class="error">
</div>
<div class="panel panel-default" id="registro">
  <div class="panel-heading">
    <h2 class="panel-title" id="tittlePanel">Formulario de registro</h2>
  </div>
  <div class="panel-body">
    <div class="row">
      <div class="col-lg-8 col-lg-offset-2" id="size">
        <h3 class="text-center" id="tittle">Registro de Usuario</h3>
        <form class="form-horizontal" role="form" action="#"  method="post" id="form-registro">
          <input type="hidden" name="" value="last" id="last">
          <input type="hidden" name="" value="{{!! csrf_token() !!}}" >
          <div class="form-group">
            <div class="col-lg-4 col-lg-offset-2 validar">
              <label class=""></label>
              <input class="form-control" value="" type="number" id="cedula" name="cedula"  data-toggle="tooltip" data-placement="top" title="C&eacute;dula" placeholder="C&eacute;dula">
            </div>
            <div class="col-lg-4 validar">
              <label class=""></label>
              <input class="form-control" value="" type="text" id="nombre" name="nombre"  data-toggle="tooltip" data-placement="top" title="Nombre" placeholder="Nombre">
            </div>
          </div>
          <div class="form-group">
            <div class="col-lg-4 col-lg-offset-2 validar">
              <label class=""></label>
              <input class="form-control" value="" type="text" id="apellido1" name="apellido1"  data-toggle="tooltip" data-placement="top" title="Primer apellido" placeholder="Primer apellido">
            </div>
            <div class="col-lg-4 validar">
              <label class=""></label>
              <input class="form-control" value="" type="text" id="apellido2" name="apellido2"  data-toggle="tooltip" data-placement="top" title="Segundo apellido" placeholder="Segundo apellido">
            </div>
          </div>
          <div class="form-group">
            <div class="col-lg-4 col-lg-offset-2 validar">
              <label class=""></label>
              <input class="form-control" value="" type="text" id="correo" name="correo"  data-toggle="tooltip" data-placement="top" title="Correo electr&oacute;nico" placeholder="Correo electr&oacute;nico @">
            </div>
            <div class="col-lg-4 validar">
              <label class=""></label>
              <input class="form-control" value="" type="number" id="telefono" name="telefono"  data-toggle="tooltip" data-placement="top" title="Telefono" placeholder="Telefono">
            </div>
          </div>
          <fieldset>
            <legend class="text-center"> <h4>Fecha de Nacimiento</h4></legend>
            <div class="form-group  text-center">
              <div class="col-lg-2 col-lg-offset-2" id="dia">
              </div>
              <div class="col-lg-3" id="mes">
            </div>
            <div class="col-lg-3" id="ano">
            </div>
          </fieldset>
          <fieldset>
            <legend class="text-center"><h4>G&eacute;nero</h4></legend>
            <div class="form-group text-center">
              <div class="col-lg-4 col-lg-offset-2 validar">
                <label class="">Masculino</label>
                <input class="form-control" type="radio" id="1" name="radio" value="0" checked="checked">
              </div>
              <div class="col-lg-4">
                <label class="">Femenino</label>
                <input class="form-control" type="radio" id="2" name="radio" value="1">
              </div>
            </div>
          </fieldset>
          <fieldset class="ocultar">
            <legend class="text-center">Inicio de sesi&oacute;n</legend>
            <div class="form-group text-center">
              <div class="col-lg-4 col-lg-offset-2 validar">
                <label class=""></label>
                <input class="form-control" type="password" id="contra1" name="contra1" value="" data-toggle="tooltip" data-placement="top" title="Contrase&ntilde;a" placeholder="Contrase&ntilde;a">
              </div>
              <div class="col-lg-4 validar">
                <label class=""></label>
                <input class="form-control" type="password" id="contra2" name="contra2" value="" data-toggle="tooltip" data-placement="top" title="Repita la contrase&ntilde;a" placeholder="Repita la contrase&ntilde;a">
              </div>
            </div>
          </fieldset>
          <fieldset class="profile">
            <legend class="text-center">Inicio de sesi&oacute;n</legend>
            <div class="form-group text-center">
              <div class="col-lg-4 col-lg-offset-2 validar">
                <label class=""></label>
                <input class="form-control" type="password" id="lastPass" name="lastPass" value="" data-toggle="tooltip" data-placement="top" title="Contrase&ntilde;a anterior" placeholder="Contrase&ntilde;a anterior">
              </div>
              <div class="col-lg-4 validar">
                <label class=""></label>
                <input class="form-control" type="password" id="newPass" name="newPass" value="" data-toggle="tooltip" data-placement="top" title="Nueva contrase&ntilde;a" placeholder="Nueva contrase&ntilde;a">
              </div>
            </div>
          </fieldset>

          <div class="form-group profile">
            <div class="col-lg-4 col-lg-offset-4">
              <label class=""></label>
              <button type="button" class="btn btn-success form-control" name="button" id="perfil">Actualizar informaci&oacute;n</button>
            </div>
          </div>
          <div class="form-group ocultar">
            <div class="col-lg-4 col-md-offset-2">
              <label class=""></label>
              <button type="button" class="btn btn-danger form-control" name="button" id="regresar">Regresar</button>
            </div>
            <div class="col-lg-4 ">
              <label class=""></label>
              <button type="button" class="btn btn-info form-control" name="button" id="registrar">Registrarme</button>
            </div>
          </div>
          <div class="form-group group-contactos">
            <div class="col-lg-4 ">
              <label class=""></label>
              <button type="button" class="btn btn-success form-control" name="button" id="add">Agregar</button>
            </div>
            <div class="col-lg-4 ">
              <label class=""></label>
              <button type="button" class="btn btn-warning form-control" name="button" id="update">Modificar</button>
            </div>
            <div class="col-lg-4 ">
              <label class=""></label>
              <button type="button" class="btn btn-danger form-control" name="button" id="delete">Eliminar</button>
            </div>
          </div>
          <div class="form-group group-contactos">
            <div class="col-lg-4 col-lg-offset-4">
              <label class=""></label>
              <button type="button" class="btn btn-primary form-control hide" name="button" id="cancel">Cancelar</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
