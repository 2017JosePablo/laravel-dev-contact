@if (!Session::has('datos'))
  <script type="text/javascript">
    window.location.href = "{{url('index?error=notSesion')}}";
  </script>
@endif
