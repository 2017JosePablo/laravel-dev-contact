function makeMessage2(type,header,body){
  var htmlResponse = "";
  htmlResponse+="<div class='page-header'>";
    htmlResponse+="<div class='alert alert-"+type+" text-center' role='alert'>";
      htmlResponse+="<h3><strong>"+header+"</strong></h3>";
      htmlResponse+="<h4>"+body+"</h5>"
    htmlResponse+="</div>";
  htmlResponse+="</div>";
  //Borrar el mensaje depues de 5 segundos
  setTimeout(function(){
    $(".error").html('');
  },9000);
  return htmlResponse;
}

function makeMessage(type,header,body){
  $.toast({
      heading: header,
      text: body,
      showHideTransition: 'slide',
      hideAfter: 5000,  // in milli seconds
      icon: type
  })
}
