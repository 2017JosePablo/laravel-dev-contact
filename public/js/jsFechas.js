var today = new Date();
var dd = today.getDate();
var mm = today.getMonth(); //January is 0!
var yyyy = today.getFullYear();


var dd2 = "";
var mm2 = "";
var yyyy2 = "";

function setDia(dia){
  dd2 = dia;
  $('#dia').html(cargarSelectDia());
}
function setMes(mes){
  mm2 = mes;
  $('#mes').html(cargarSelectMes());
}
function setAno(ano){
  yyyy2 = ano;
  $('#ano').html(cargarSelectAno());
}

function cargarSelectDia(){
  var dias = "<label></label>";
  dias += "<select  class='form-control ' name = 'listaDias' id='listaDias' data-toggle='tooltip' data-placement='top' title='Dia'>";
  for (var i = 1; i <=31; i++) {
    if (dd2.length>0 && i == dd2 || i == dd && dd2.length == 0)
      dias = dias + "<option selected>"+i+"</option>";
    else
        dias = dias + "<option>"+i+"</option>";
  }
  return dias= dias+"</select>";
}
function cargarSelectMes(){
  var mes = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
  var meses = "<label></label>";
  meses += "<select class='form-control'  name='listaMeses' id='listaMeses' data-toggle='tooltip' data-placement='top' title='Mes'>";
  for (var i = 0; i < mes.length; i++) {
    if (mm2.length > 0 && i == mm2-1 || i == mm && mm2.length == 0 )
      meses = meses + "<option value="+(i+1)+" selected>"+mes[i]+"</option>";
    else
      meses = meses + "<option value="+(i+1)+" >"+mes[i]+"</option>";
  }
  return meses = meses+"</select>";
}
function cargarSelectAno(){
  var anos = "<label></label>";
  anos += "<select class='form-control' name='listadoAnos' id='listadoAnos' data-toggle='tooltip' data-placement='top' title='Año'>";
  for (var i = 1950; i <= yyyy; i++) {
    if (yyyy2.length > 0 && i == yyyy2 || i == yyyy && yyyy2.length == 0)
      anos = anos + "<option selected>"+i+"</option>";
    else
        anos = anos + "<option>"+i+"</option>";
  }
  return anos = anos +"</select>";
}
