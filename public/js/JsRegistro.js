var Genero = 0;

$(document).ready(function(){
  $('.profile').hide();
  $('.group-contactos').hide();

  $('#dia').html(cargarSelectDia());
  $('#mes').html(cargarSelectMes());
  $('#ano').html(cargarSelectAno());

  $("#registrar").click(function(){
    if($("#form-registro").valid()){
      registrarUsuario();
    }
  });
  $('input[name="radio"]').click(function(){
    Genero = $(this).val();
  });
  $('#regresar').click(function(){
    window.location = 'index';
  });
  validarForm();
});

function validarForm(){
  $( "#form-registro" ).validate( {
    rules: {
      cedula:{
        required: true,
        minlength: 9,
        maxlength: 9,
        number:true
      },
      nombre:{
        required: true,
        minlength: 3
      },
      correo:{
        required: true,
        email: true
      },
      apellido1:{
        required: true,
        minlength: 3
      },
      apellido2:{
        required: true,
        minlength: 3
      },
      telefono:{
        required: true,
        minlength: 8,
        maxlength: 8,
        number:true
      },
      contra1:{
        required: true,
        minlength:5
      },
      contra2:{
        required: true,
        minlength:5,
        equalTo: "#contra1"
      }
    },
    messages: {
      cedula:{
          required: "Este campo es obligatorio",
          minlength: "Debe tener como m&iacute;nimo 9 caracteres",
          number: "Solamente se permiten numeros",
          maxlength: "Debe tener como m&aacute;ximo 9 caracteres"
      },
      nombre:{
        required: "Este campo es obligatorio",
        minlength: "Debe tener como m&iacute;nimo 3 caracteres"
      },
      correo:{
        required:"Este campo es obligatorio",
        email:"Correo no valido"
      },
      apellido1:{
          required:"Este campo es obligatorio",
          minlength: "Debe tener como m&iacute;nimo 3 caracteres"
      },
      apellido2:{
        required:"Este campo es obligatorio",
        minlength: "Debe tener como m&iacute;nimo 3 caracteres"
      },
      telefono:{
        required:"Este campo es obligatorio",
        minlength: "Debe tener como m&iacute;nimo 8 caracteres",
        number: "Solamente se permiten numeros",
        maxlength: "Debe tener como m&aacute;ximo 8 caracteres"
      },
      contra1:{
        required: "Este campo es obligatorio",
        minlength: "Debe tener como m&iacute;nimo 5 caracteres",
      },
      contra2:{
        required: "Este campo es obligatorio",
        minlength: "Debe tener como m&iacute;nimo 5 caracteres",
        equalTo: "No son iguales las contrase&ntilde;as"
      }
    },
    errorElement: "em",
    errorPlacement: function ( error, element ) {
      // Add the `help-block` class to the error element
      error.addClass( "help-block" );

      if ( element.prop( "type" ) === "radio" ) {
        error.insertAfter( element.parent( "label" ) );
      } else {
        error.insertAfter( element );
      }
    },
    highlight: function ( element, errorClass, validClass ) {
      $( element ).parents( ".validar" ).addClass( "has-error" ).removeClass( "has-success" );
    },
    unhighlight: function (element, errorClass, validClass) {
      $( element ).parents( ".validar" ).addClass( "has-success" ).removeClass( "has-error" );
    }
  });
}
function registrarUsuario(){

  var dia   = $('#listaDias').val();
  var mes   = document.getElementById("listaMeses").selectedIndex+1;
  var ano   = $('#listadoAnos').val();
  var newFecha = ano+"-"+mes+"-"+dia;
	$.ajax({
      type:'POST',
      url:'regUsuario',
      //dataType: "json",
      data:{"_token":$('meta[name="_token"]').attr('content'),
      cedula:$('#cedula').val(),
      nombre:$('#nombre').val(),
      apellido1:$('#apellido1').val(),
      apellido2:$('#apellido2').val(),
      correo:$('#correo').val(),
      telefono:$('#telefono').val(),
      fecha: newFecha,
      genero:Genero,
      contrasena:$('#contra1').val(),
      rol:2,
  		},
       success:function(data){
         //Viene un id de usuario ejemplo 1
         if(data == 'true'){
           //alert("Se ha insertado");

           $('#form-registro').trigger("reset");
           window.location = 'index?success=inserted';
         }else{
          if(data == 'notInserted'){
            $('.error').html(makeMessage('error','Error','Vaya, ha ocurrido un error insertando los datos'));
          }else{
            if(data == 'notLogin'){
              $('.error').html(makeMessage('error','Error','Vaya, ha ocurrido un error insertando los datos'));
            }else{
              if(data == 'userExist'){
                  $('.error').html(makeMessage('error','Usuario existe','Vaya, este usuario ya se encuentra registrado'));
              }else if (data == 'true') {
                $('.error').html(makeMessage('success','Felicidades','Se han guardado los datos satisfactoriamente'));
              }
            }
          }
        }
       },
       error: function(xhr, status, error){
         console.log(xhr+" "+status+" "+error);
       },
       beforeSend: function( xhr ) {
       }
		});
	}
