$(document).ready(function(){
  $("#acceder").click(function(){
    if ($("#form").valid()) {
      iniciarSesion();
    }
  });
  validarForm();

  $('#reg').click(function(){
    window.location = 'registro';
  })
});
function validarForm(){
  $( "#form" ).validate( {
    rules: {
      usuario:{
        required: true,
        minlength: 9,
        maxlength: 9,
        number: true
      },
      contra:{
        required: true,
        minlength: 5
      }
    },
    messages: {
      usuario:{
          required: "Este campo es obligatorio",
          minlength: "Debe tener como m&iacute;nimo 9 caracteres",
          maxlength: "Debe tener como m&aacute;ximo 9 caracteres",
          number: "Solamente se permiten numeros"
      },
      contra:{
        required: "Este campo es obligatorio",
        minlength: "Debe tener como m&iacute;nimo 5 caracteres"
      }
    },
    errorElement: "em",
    errorPlacement: function ( error, element ) {
      error.addClass( "help-block" );
      error.insertAfter( element );
    },
    highlight: function ( element, errorClass, validClass ) {
      $( element ).parents( ".col-lg-8" ).addClass( "has-error" ).removeClass( "has-success" );
    },
    unhighlight: function (element, errorClass, validClass) {
      $( element ).parents( ".col-lg-8" ).addClass( "has-success" ).removeClass( "has-error" );
    }
  });
}
function iniciarSesion(){
	var usuario = $('#usuario').val();
	var contrasena = $('#contra').val();
	$.ajax({
      type:'POST',
      url:'iniciar',
      data:{"_token":$('meta[name="_token"]').attr('content'),
  		usuario:usuario,
  		contrasena:contrasena,
  		},
       success:function(data){
         if(data === 'false'){
           $('.error').html(makeMessage('error','Datos inv&aacute;lidos','Vaya, al parecer aun no tienes una cuenta, debes registrarse antes de iniciar sesi&oacute;n'));
         }else{
           window.location = 'contactos';
         }
       }
		});
	}
