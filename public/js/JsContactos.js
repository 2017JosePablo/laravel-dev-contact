var Genero = 1;
var idPersona = 0;
$(document).ready(function(){
  //Limpiar el formulario de registro
  $('#form-registro').trigger("reset");

  $('#tittle').html('Registro de contacto');
  //Cargar la tabla
  getContactos();

  $('#dia').html(cargarSelectDia());
  $('#mes').html(cargarSelectMes());
  $('#ano').html(cargarSelectAno());

  $('.ocultar').hide();
  $('.profile').hide();

  $("#size" ).addClass( "col-lg-12" ).removeClass( "col-lg-8 col-lg-offset-2");



  $('input[name="radio"]').click(function(){
    Genero = $(this).val();
  });

  validarForm();

  $('#cancel').click(function(){
    enableDisable();
  });
  $("#delete").addClass( "disabled");
  $("#update").addClass( "disabled");

  $("#add").click(function(){
    if($("#form-registro").valid()){
      registrarUsuario();
    }
  });
  $("#update").click(function(){
    if ($('#form-registro').valid()) {
      actualizarUsuario();
    }
  });
  $('#delete').click(function(){
    bootbox.confirm({
    message: "Desea eliminar a este contacto?",
    buttons: {
        confirm: {
            label: 'Eliminar',
            className: 'btn-danger'
        },
        cancel: {
            label: 'Cancelar',
            className: 'btn-info'
        }
    },
    callback: function (result) {
      if(result){
        eliminarUsuario();
      }
    }
    });
  })
});
function enableDisable(){
  $('#form-registro').trigger("reset");
  $("#add").removeClass( "disabled");
  $("#delete").addClass( "disabled");
  $("#update").addClass( "disabled");
  $('#cancel').addClass("hide");
}
function registrarUsuario(){
  var dia   = $('#listaDias').val();
  var mes   = document.getElementById("listaMeses").selectedIndex+1;
  var ano   = $('#listadoAnos').val();
  var newFecha = ano+"-"+mes+"-"+dia;
	$.ajax({
      type:'POST',
      url:'registerContact',
      //dataType: "json",
      data:{"_token":$('meta[name="_token"]').attr('content'),
      cedula:$('#cedula').val(),
      nombre:$('#nombre').val(),
      apellido1:$('#apellido1').val(),
      apellido2:$('#apellido2').val(),
      correo:$('#correo').val(),
      telefono:$('#telefono').val(),
      fecha: newFecha,
      genero: Genero,
  		},
       success:function(data){

         if(data == 'userExist'){
             $('.error').html(makeMessage('error','Usuario existe','Vaya, este usuario ya se encuentra registrado'));
         }else if (data == 'true') {
           $('.error').html(makeMessage('success','Felicidades','Se han guardado los datos satisfactoriamente'));
           getContactos();
         }else if (data == 'false') {
           $('.error').html(makeMessage('error','Error','Vaya, ha ocurrido un error al guardar los datos'));
         }
         $('#form-registro').trigger("reset");
       },
       error: function(xhr, status, error){
         console.log(xhr+" "+status+" "+error);
       },
       beforeSend: function( xhr ) {
       }
		});
	}
  function actualizarUsuario(){
    var dia   = $('#listaDias').val();
    var mes   = document.getElementById("listaMeses").selectedIndex+1;
    var ano   = $('#listadoAnos').val();
    var newFecha = ano+"-"+mes+"-"+dia;
  	$.ajax({
        type:'POST',
        url:'actualizarContact',
        //dataType: "json",
        data:{"_token":$('meta[name="_token"]').attr('content'),
        id: idPersona,
        cedula:$('#cedula').val(),
        nombre:$('#nombre').val(),
        apellido1:$('#apellido1').val(),
        apellido2:$('#apellido2').val(),
        correo:$('#correo').val(),
        telefono:$('#telefono').val(),
        fecha: newFecha,
        genero: Genero,
    		},
         success:function(data){
           if (data == 'true') {
             $('.error').html(makeMessage('success','Felicidades','Se han actualizado los datos'));
             getContactos();
             enableDisable();
           }else if (data == 'false') {
             $('.error').html(makeMessage('error','Error','Vaya, ha ocurrido un error al actualizar los datos, debes realizar algún cambio para actualizar'));
           }
         },
         error: function(xhr, status, error){
           console.log(xhr+" "+status+" "+error);
         },
         beforeSend: function( xhr ) {
         }
  		});
  	}
    function eliminarUsuario(){
    	$.ajax({
          type:'POST',
          url:'deleteContact',
          //dataType: "json",
          data:{"_token":$('meta[name="_token"]').attr('content'),
          id: idPersona,
      		},
           success:function(data){
             console.log(data);
             if (data == 'true') {
               $('.error').html(makeMessage('success','Felicidades','Se ha eliminado los datos'));
               getContactos();
               enableDisable();
             }else if (data == 'false') {
               $('.error').html(makeMessage('error','Error','Vaya, ha ocurrido un error al eliminar los datos'));
             }
           },
           error: function(xhr, status, error){
             console.log(xhr+" "+status+" "+error);
           },
           beforeSend: function( xhr ) {
           }
    		});
    	}
    function getContactos(){
      $.ajax({
          type:'POST',
          url:'getContact',
          data:{"_token":$('meta[name="_token"]').attr('content')},
           success:function(data){
             if (data.length != 0) {
               var htmlResponse = "";
               for(var i in data){
                 //<button onclick="cargarDatos('+id+','+nombre+','+correo+','+telefono+','+edad+','+provincia+','+genero+')"  id='+data[i].id+'>'+data[i].nombre+'</button>
                 htmlResponse+= "<tr>";
                  htmlResponse+='<td class="text-center"><button onclick="cargarDatos('+"'"+data[i].id+"'"+','+data[i].ced+','+"'"+data[i].nom+"'"+','+"'"+data[i].ap1+"'"+','+"'"+data[i].ap2+"'"+','+"'"+data[i].corr+"'"+','+"'"+data[i].tel+"'"+','+"'"+data[i].fec+"'"+','+"'"+data[i].gen+"'"+')"  type="button" class="btn btn-info btn-block">'+data[i].nom+" "+data[i].ap1+" "+data[i].ap2+'</button></td>';
                htmlResponse+= "</tr>";
               }
             }else{
               htmlResponse+= "<tr>";
                htmlResponse+='<td class="text-center"><button  type="button" class="btn btn-danger btn-block disabled">No hay contactos disponibles</button></td>';
              htmlResponse+= "</tr>";
             }
             $('#tbody').html(htmlResponse);
           },
           error: function(xhr, status, error){
             console.log(xhr+" "+status+" -- "+error);
           },
           beforeSend: function( xhr ) {
             //console.log(xhr);
           }
    		});
    }
    function cargarDatos(id,ced,nom,ap1,ap2,corr,tel,fec,gen){

      idPersona = id;

      setDia(fec.split('-')[2]);
      setMes(fec.split('-')[1]);
      setAno(fec.split('-')[0]);
      $('#cedula').val(ced);
      $('#nombre').val(nom);
      $('#apellido1').val(ap1);
      $('#apellido2').val(ap2);
      $('#correo').val(corr);
      $('#telefono').val(tel);
      $('input[name="radio"][value="'+gen+'"]').prop("checked", true);

      $("#add").addClass( "disabled");
      $("#delete").removeClass( "disabled");
      $("#update").removeClass( "disabled" )
      $('#cancel').removeClass('hide');
    }

    function validarForm(){
      $( "#form-registro" ).validate( {
        rules: {
          cedula:{
            required: true,
            minlength: 9,
            maxlength: 9,
            number:true
          },
          nombre:{
            required: true,
            minlength: 3
          },
          correo:{
            required: true,
            email: true
          },
          apellido1:{
            required: true,
            minlength: 3
          },
          apellido2:{
            required: true,
            minlength: 3
          },
          telefono:{
            required: true,
            minlength: 8,
            maxlength: 8,
            number:true
          }
        },
        messages: {
          cedula:{
              required: "Este campo es obligatorio",
              minlength: "Debe tener como m&iacute;nimo 9 caracteres",
              number: "Solamente se permiten numeros",
              maxlength: "Debe tener como m&aacute;ximo 9 caracteres"
          },
          nombre:{
            required: "Este campo es obligatorio",
            minlength: "Debe tener como m&iacute;nimo 3 caracteres"
          },
          correo:{
            required:"Este campo es obligatorio",
            email:"Correo no valido"
          },
          apellido1:{
              required:"Este campo es obligatorio",
              minlength: "Debe tener como m&iacute;nimo 3 caracteres"
          },
          apellido2:{
            required:"Este campo es obligatorio",
            minlength: "Debe tener como m&iacute;nimo 3 caracteres"
          },
          telefono:{
            required:"Este campo es obligatorio",
            minlength: "Debe tener como m&iacute;nimo 8 caracteres",
            number: "Solamente se permiten numeros",
            maxlength: "Debe tener como m&aacute;ximo 8 caracteres"
          }
        },
        errorElement: "em",
        errorPlacement: function ( error, element ) {
          // Add the `help-block` class to the error element
          error.addClass( "help-block" );

          if ( element.prop( "type" ) === "radio" ) {
            error.insertAfter( element.parent( "label" ) );
          } else {
            error.insertAfter( element );
          }
        },
        highlight: function ( element, errorClass, validClass ) {
          $( element ).parents( ".validar" ).addClass( "has-error" ).removeClass( "has-success" );
        },
        unhighlight: function (element, errorClass, validClass) {
          $( element ).parents( ".validar" ).addClass( "has-success" ).removeClass( "has-error" );
        }
      });
    }
