var Genero = 1;
$(function() {
  $('.ocultar').hide();
  $('.group-contactos').hide();
  getInfoPerfil();
  $('#tittle').html('Actualizar informaci&oacute;n de perfil');
  $('#tittlePanel').html('Formulario de actualizaci&oacute;n');

  validarFormProfile();

  $('#perfil').click(function(){
    if($("#form-registro").valid()){
        actualizarUsuario();
    }
  });

  $('input[name="radio"]').click(function(){
    Genero = $(this).val();
  });

});
function isPasswordPresent() {
    return $('#lastPass').val().length > 0;
}
function validarFormProfile(){
  $( "#form-registro" ).validate( {
    rules: {
      cedula:{
        required: true,
        minlength: 9,
        maxlength: 9,
        number:true
      },
      nombre:{
        required: true,
        minlength: 3
      },
      correo:{
        required: true,
        email: true
      },
      apellido1:{
        required: true,
        minlength: 3
      },
      apellido2:{
        required: true,
        minlength: 3
      },
      telefono:{
        required: true,
        minlength: 8,
        maxlength: 8,
        number:true
      },
      lastPass: {
          minlength: {
              depends: isPasswordPresent,
              param: 5
          },
          equalTo: {
              depends: isPasswordPresent,
              param: "#last"
          }
      },
      newPass: {
          required: isPasswordPresent,
          minlength: {
              depends: isPasswordPresent,
              param: 5
          }
      },
    },
    messages: {
      cedula:{
          required: "Este campo es obligatorio",
          minlength: "Debe tener como m&iacute;nimo 9 caracteres",
          number: "Solamente se permiten numeros",
          maxlength: "Debe tener como m&aacute;ximo 9 caracteres"
      },
      nombre:{
        required: "Este campo es obligatorio",
        minlength: "Debe tener como m&iacute;nimo 3 caracteres"
      },
      correo:{
        required:"Este campo es obligatorio",
        email:"Correo no valido"
      },
      apellido1:{
          required:"Este campo es obligatorio",
          minlength: "Debe tener como m&iacute;nimo 3 caracteres"
      },
      apellido2:{
        required:"Este campo es obligatorio",
        minlength: "Debe tener como m&iacute;nimo 3 caracteres"
      },
      telefono:{
        required:"Este campo es obligatorio",
        minlength: "Debe tener como m&iacute;nimo 8 caracteres",
        number: "Solamente se permiten numeros",
        maxlength: "Debe tener como m&aacute;ximo 8 caracteres"
      },
      lastPass:{
        minlength: "Debe tener como m&iacute;nimo 5 caracteres",
        equalTo: "La contrase&ntilde;a no es igual a la anterior"
      },
      newPass:{
        minlength: "Debe tener como m&iacute;nimo 5 caracteres",
        required: "Este campo es obligatorio"
      }
    },
    errorElement: "em",
    errorPlacement: function ( error, element ) {
      // Add the `help-block` class to the error element
      error.addClass( "help-block" );

      if ( element.prop( "type" ) === "radio" ) {
        error.insertAfter( element.parent( "label" ) );
      } else {
        error.insertAfter( element );
      }
    },
    highlight: function ( element, errorClass, validClass ) {
      $( element ).parents( ".validar" ).addClass( "has-error" ).removeClass( "has-success" );
    },
    unhighlight: function (element, errorClass, validClass) {
      $( element ).parents( ".validar" ).addClass( "has-success" ).removeClass( "has-error" );
    }
  });
}
function getInfoPerfil(){
	$.ajax({
      type:'POST',
      url:'infoPerfil',
      data:{"_token":$('meta[name="_token"]').attr('content'),
  		},
       success:function(data){
         setDia(data[0].fec.split('-')[2]);
         setMes(data[0].fec.split('-')[1]);
         setAno(data[0].fec.split('-')[0]);
         $('#cedula').val(data[0].ced);
         $('#nombre').val(data[0].nom);
         $('#apellido1').val(data[0].ap1);
         $('#apellido2').val(data[0].ap2);
         $('#correo').val(data[0].corr);
         $('#telefono').val(data[0].tel);
         $('input[name="radio"][value="'+data[0].gen+'"]').prop("checked", true);
         $('#last').val(data[0].contr);
       },
       error: function(xhr, status, error){
         console.log(xhr+" "+status+" "+error);
       },
       beforeSend: function( xhr ) {
       }
		});
}
function actualizarUsuario(){
  var dia   = $('#listaDias').val();
  var mes   = document.getElementById("listaMeses").selectedIndex+1;
  var ano   = $('#listadoAnos').val();
  var newFecha = ano+"-"+mes+"-"+dia;

  var change = "false";
  if(isPasswordPresent()){
    change = "true";
  }

  $.ajax({
      type:'POST',
      url:'actualizarPersona',
      data:{"_token":$('meta[name="_token"]').attr('content'),

      cedula:$('#cedula').val(),
      nombre:$('#nombre').val(),
      apellido1:$('#apellido1').val(),
      apellido2:$('#apellido2').val(),
      correo:$('#correo').val(),
      telefono:$('#telefono').val(),
      fecha: newFecha,
      genero: Genero,
      state: change,
      cont: $('#newPass').val(),

      },
       success:function(data){
         if(data['user']== 'false'  && data['pass'].length == 0 || data['user']== 'false' && data['pass'] == 'false'){
           $('.error').html(makeMessage('error','Vaya un error!!','Ha ocurrido un error al actualizar los datos, debes realizar algún cambio para actualizar'));
         }else {
           if (data['user'] == 'true' && data['pass'] == 'true' || data['user'] == 'true' && data['pass'].length == 0 || data['user'] == 'false' && data['pass'] == 'true') {
              $('.error').html(makeMessage('success','Felicidades','Se han actualizado los datos'));
           }
         }
       },
       error: function(xhr, status, error){
         console.log(xhr+" "+status+" "+error);
       },
       beforeSend: function( xhr ) {
       }
    });
  }
